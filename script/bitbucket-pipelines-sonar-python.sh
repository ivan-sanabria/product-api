#!/usr/bin/env bash

PROJECT_PATH=$(pwd)

# Verify and install requirements for the application
python --version
pip install coverage
pip install -r requirements.txt

# Coverage commands to report sonar cloud
coverage erase
coverage run --source=. -m unittest discover -s . -p "*_test.py"
coverage xml -i

# Retrieves sonar scanner for analysis
cd /opt
apt-get update && apt-get install -y jq unzip
curl --insecure -OL https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.3.0.1492-linux.zip
unzip sonar-scanner-cli-3.3.0.1492-linux.zip

./sonar-scanner-3.3.0.1492-linux/bin/sonar-scanner -Dsonar.projectBaseDir=${PROJECT_PATH} -Dsonar.login=${SONAR_LOGIN}