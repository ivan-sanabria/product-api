# Serverless Web API for Kaffeeland Product API

version 1.0.0 - 19/06/2019

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/kaffeeland-product-api.svg)](http://bitbucket.org/ivan-sanabria/kaffeeland-product-api/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/kaffeeland-product-api.svg)](http://bitbucket.org/ivan-sanabria/kaffeeland-product-api/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-product-api&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_kaffeeland-product-api)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-product-api&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_kaffeeland-product-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-product-api&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_kaffeeland-product-api)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-product-api&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_kaffeeland-product-api)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-product-api&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_kaffeeland-product-api)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-product-api&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_kaffeeland-product-api)

## Specifications

This is a web api that uses Python 3.6 runtime for AWS Lambda with Amazon API Gateway to proxy RESTful API 
requests to the Lambda function.

It exposes the functionality of the following requirements:

1. Store products for kaffeeland application.
2. Update products for kaffeeland application.
3. Get product with specific identifier for kaffeeland application.
4. List all the products for kaffeeland application.

## Requirements

- Python 3.6.8
- Serverless 1.38.x

## Serverless Setup

In order to deploy the application in AWS account is required to have an **AWSAccessKeyId**
and **AWSSecretKey**.

To setup and configure serverless framework, for deploying the stack using AWS CloudFormation is 
required to execute the following commands on the root folder of the project:

```bash
npm install -g serverless
serverless plugin install -n serverless-python-requirements
serverless plugin install -n serverless-wsgi
serverless config credentials --provider aws --key ${KEY} --secret ${SECRET} --profile demos
```

## Check Application Test Coverage on Terminal

Under the root folder, you could execute the following commands to check coverage locally:

```bash
pip install coverage
coverage erase
coverage run --source=. -m unittest discover -s . -p "*_test.py"
coverage html -i
open htmlcov/index.html
```

## Deployment Application AWS on Terminal using Serverless

After see the test coverage and generate files let's proceed to deploy the application in
AWS Lambda to serve requests:

```bash
serverless deploy -v --aws-profile demos
```

## Test Production Application Locally

After deployment is successful, there are 6 endpoints exposed:

- GET  - https://${aws-temporal-domain}/production/
- GET  - https://${aws-temporal-domain}/production/product
- POST - https://${aws-temporal-domain}/production/product
- GET  - https://${aws-temporal-domain}/production/product/{product_id}
- PUT  - https://${aws-temporal-domain}/production/product/{product_id}
- DELETE  - https://${aws-temporal-domain}/production/product/{product_id}

To test the endpoints you could use the following commands:

```bash
curl -X GET https://${aws-temporal-domain}/production/
curl -X GET https://${aws-temporal-domain}/production/product
curl -X POST -d '{"name": "Sello Rojo", "country": "Colombia", "altitude": 1430, "preparation": "Moka Pot", "mixers": ["Milk", "Honey"]}' https://${aws-temporal-domain}/production/product
curl -X GET https://${aws-temporal-domain}/production/product/{product_id}
curl -X PUT -d  '{"country": "Brazil", "altitude": 1110}' https://${aws-temporal-domain}/production/product/{product_id}
curl -X DELETE https://${aws-temporal-domain}/production/product/{product_id}
```

# Contact Information

Email: icsanabriar@googlemail.com