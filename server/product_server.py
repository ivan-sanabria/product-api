"""
   Copyright 2019 Iván Camilo Sanabria

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from flask import Flask, abort
from flask_restful import Resource, Api, reqparse

app = Flask(__name__)
app.config['BUNDLE_ERRORS'] = True

api = Api(app)

# Mock datasource of products.
products = {
    '1': {'name': 'El Pamichal', 'country': 'Colombia', 'altitude': 1560, 'preparation': 'Moka Pot',
          'mixers': ['Butter', 'Vanilla', 'Ice Cream']},
    '2': {'name': 'Las Taguas', 'country': 'Colombia', 'altitude': 2200, 'preparation': 'French Press',
          'mixers': ['Milk']},
    '3': {'name': 'Sierra Nevada', 'country': 'Colombia', 'altitude': 1200, 'preparation': 'Chemex',
          'mixers': ['Whiskey']}
}


# Not found response given when the product identifier is not found on the datasource.
def abort_if_product_doesnt_exist(product_id):
    if product_id not in products:
        abort(404, description="Product {} does not exist.".format(product_id))


# Define request parser for products.
parser = reqparse.RequestParser(bundle_errors=True)

parser.add_argument('name', required=True, location='json')
parser.add_argument('country', required=True, location='json')
parser.add_argument('altitude', required=True, location='json', type=int)
parser.add_argument('preparation', required=True, location='json')
parser.add_argument('mixers', location='json', action='append')


# Define the status page
class Status(Resource):
    def get(self):
        return {'status': 'Running'}


# Define product as resource to support GET / PUT / DELETE operations with product_id as path parameter.
class Product(Resource):

    def get(self, product_id):
        abort_if_product_doesnt_exist(product_id)
        return products[product_id]

    def delete(self, product_id):
        abort_if_product_doesnt_exist(product_id)
        del products[product_id]
        return '', 204

    def put(self, product_id):
        args = parser.parse_args()
        product = {'name': args['name'], 'country': args['country'], 'altitude': args['altitude'],
                   'preparation': args['preparation'], 'mixers': args['mixers']}
        products[product_id] = product
        return product, 202


# Define product list resource to support GET / POST operations.
class ProductList(Resource):

    def get(self):
        return products

    def post(self):
        args = parser.parse_args()
        product_id = str(int(max(products.keys())) + 1)
        products[product_id] = {'name': args['name'], 'country': args['country'], 'altitude': args['altitude'],
                                'preparation': args['preparation'], 'mixers': args['mixers']}
        return products[product_id], 201


# Define resources for product api.
api.add_resource(Status, '/')
api.add_resource(ProductList, '/product')
api.add_resource(Product, '/product/<product_id>')

# Define main section to run application.
if __name__ == "__main__":
    app.run(debug=False)
