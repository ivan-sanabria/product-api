"""
   Copyright 2019 Iván Camilo Sanabria

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

import json
import unittest

from .product_server import app


# Class that contains the test suite for GET operations.
class GetTestSuite(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_get_book_1_exists(self):
        # Precondition that product 2 exists
        resp = self.app.get('/product/2')
        # Validation of response
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content_type, 'application/json')
        # Validation of content
        content = json.loads(resp.get_data(as_text=True))
        self.assertEqual(content['name'], 'Las Taguas')
        self.assertEqual(content['country'], 'Colombia')
        self.assertEqual(content['altitude'], 2200)
        self.assertEqual(content['preparation'], 'French Press')
        self.assertEqual(content['mixers'], ['Milk'])

    def test_get_book_10_not_exists(self):
        # Precondition that product 10 not exists
        resp = self.app.get('/product/10')
        # Validation of response
        self.assertEqual(resp.status_code, 404)
        self.assertEqual(resp.content_type, 'application/json')
        # Validation of content
        content = json.loads(resp.get_data(as_text=True))
        self.assertEqual(content['message'], 'Product 10 does not exist.')

    def test_get_book_aa_not_exists(self):
        # Precondition that product aa not exists
        resp = self.app.get('/product/aa')
        # Validation of response
        self.assertEqual(resp.status_code, 404)
        self.assertEqual(resp.content_type, 'application/json')
        # Validation of content
        content = json.loads(resp.get_data(as_text=True))
        self.assertEqual(content['message'], 'Product aa does not exist.')

    def test_get_books(self):
        # Precondition that products exists
        resp = self.app.get('/product')
        # Validation of response
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.content_type, 'application/json')
        # Validation of content
        content = json.loads(resp.get_data(as_text=True))
        self.assertGreaterEqual(len(content.keys()), 2)


# Class that contains the test suite for POST operations.
class PostTestSuite(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

        self.valid_payload = {
            'name': 'Sello Rojo',
            'country': 'Colombia',
            'altitude': 1430,
            'preparation': 'Moka Pot',
            'mixers': ['Milk', 'Honey']
        }

        self.invalid_payload = {
            "id": "Hello World!"
        }

    def test_post_book_valid_payload(self):
        # Precondition that product payload is valid
        resp = self.app.post('/product', data=json.dumps(self.valid_payload), content_type='application/json')
        # Validation of response
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(resp.content_type, 'application/json')
        # Validation of content
        content = json.loads(resp.get_data(as_text=True))
        self.assertEqual(content['name'], 'Sello Rojo')
        self.assertEqual(content['country'], 'Colombia')
        self.assertEqual(content['altitude'], 1430)
        self.assertEqual(content['preparation'], 'Moka Pot')
        self.assertEqual(content['mixers'], ['Milk', 'Honey'])

    def test_post_book_invalid_payload(self):
        # Precondition that product payload is invalid
        resp = self.app.post('/product', data=json.dumps(self.invalid_payload), content_type='application/json')
        # Validation of response
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(resp.content_type, 'application/json')


# Class that contains the test suite for PUT operations.
class PutTestSuite(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

        self.valid_payload = {
            'name': 'Aguila Roja',
            'country': 'Colombia',
            'altitude': 1130,
            'preparation': 'AeroPress',
            'mixers': ['Sugar']
        }

        self.invalid_payload = {
            "id": "Hello World!"
        }

    def test_put_book_valid_payload(self):
        # Precondition that product payload is valid
        resp = self.app.put('/product/3', data=json.dumps(self.valid_payload), content_type='application/json')
        # Validation of response
        self.assertEqual(resp.status_code, 202)
        self.assertEqual(resp.content_type, 'application/json')
        # Validation of content
        content = json.loads(resp.get_data(as_text=True))
        self.assertEqual(content['name'], 'Aguila Roja')
        self.assertEqual(content['country'], 'Colombia')
        self.assertEqual(content['altitude'], 1130)
        self.assertEqual(content['preparation'], 'AeroPress')
        self.assertEqual(content['mixers'], ['Sugar'])

    def test_put_book_invalid_payload(self):
        # Precondition that product payload is invalid
        resp = self.app.put('/product/3', data=json.dumps(self.invalid_payload), content_type='application/json')
        # Validation of response
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(resp.content_type, 'application/json')


# Class that contains the test suite for DELETE operations.
class DeleteTestSuite(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_delete_book_1_exists(self):
        # Precondition that product 1 exists
        resp = self.app.get('/product/1')
        self.assertEqual(resp.status_code, 200)
        # Execute delete operation
        resp = self.app.delete('/product/1')
        # Validation of response
        self.assertEqual(resp.status_code, 204)
        self.assertEqual(resp.content_type, 'application/json')

    def test_delete_book_10_not_exists(self):
        # Precondition that product 10 not exists
        resp = self.app.get('/product/10')
        self.assertEqual(resp.status_code, 404)
        # Execute delete operation
        resp = self.app.delete('/product/10')
        # Validation of response
        self.assertEqual(resp.status_code, 404)
        self.assertEqual(resp.content_type, 'application/json')


# This lines are required to execute the tests on CI/CD
if __name__ == '__main__':
    unittest.main()
